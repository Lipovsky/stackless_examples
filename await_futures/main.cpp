#include <iostream>

#include <experimental/coroutine>

#include <await/futures/core/promise.hpp>
#include <await/executors/tp/thread_pool.hpp>
#include <await/futures/util/async.hpp>
#include <await/futures/combine/first_of.hpp>
#include <await/time/time_keeper.hpp>

#include "futures.hpp"

using await::futures::AsyncVia;
using await::futures::Future;
using await::futures::Promise;
using namespace await::executors;

using std::this_thread::sleep_for;

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

Future<void> After(std::chrono::nanoseconds delay) {
  static await::time::TimeKeeper tk;
  return tk.After(delay);
}

auto operator co_await(std::chrono::nanoseconds delay) {
  return await::futures::FutureAwaiter<void>(After(delay));
}

//////////////////////////////////////////////////////////////////////

Future<int> Coro(ThreadPool& pool) {
  auto f1 = AsyncVia(pool, []() {
    sleep_for(1s);
    return 7;
  });

  auto f2 = AsyncVia(pool, []() {
    sleep_for(2s);
    return 13;
  });

  auto first_of = FirstOf(std::move(f1), std::move(f2));

  int value = co_await std::move(first_of);

  std::cout << "First async value: " << value << std::endl;

  co_await 5s;

  co_return value;
}

int main() {
  ThreadPool pool{4};

  auto f = Coro(pool);

  std::cout << std::move(f).GetResult().ValueOrThrow() << std::endl;

  pool.WaitIdle();
  pool.Stop();

  return 0;
}
