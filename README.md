# Stackless coroutines

## Examples

### Hand-crafted 

- [1. State machine](state_machine/main.cpp)
- [2. Await expression](state_machine_2/main.cpp)
- [3. Awaiter](state_machine_3/main.cpp)
- [4. Promise type](state_machine_4/main.cpp)

### C++20 coroutines

- [Futures](await_futures/main.cpp)
- [StaticThreadPool](thread_pool/main.cpp)
- [Echo](echo/server.cpp)
- [Compose](compose/main.cpp)

## References

- https://en.cppreference.com/w/cpp/language/coroutines
- https://lewissbaker.github.io/

### C++ Standard

- https://eel.is/c++draft/dcl.fct.def.coroutine
- https://eel.is/c++draft/expr.await