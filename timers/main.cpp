#include <asio.hpp>

#include "../promises/void.hpp"

#include <coroutine>

#include <chrono>
#include <iostream>

using namespace std::chrono_literals;

asio::io_context io_context;

//////////////////////////////////////////////////////////////////////

struct TimerAwaiter {
  asio::steady_timer& timer;

  bool await_ready() {
    return false;
  }

  void await_suspend(std::coroutine_handle<> h) {
    timer.async_wait([h](std::error_code) mutable {
      h.resume();
    });
  }

  void await_resume() {
  }
};

auto operator co_await(asio::steady_timer& timer) {
  return TimerAwaiter{timer};
}

//////////////////////////////////////////////////////////////////////

void Coro() {
  asio::steady_timer timer{io_context};

  timer.expires_after(2s);
  co_await timer;

  std::cout << "Hi!" << std::endl;
}

int main() {
  // Start coroutine
  Coro();

  // Run event loop
  io_context.run();

  return 0;
}
