cmake_minimum_required(VERSION 3.17)
project(coroutines_ts)

# Compile options

set(CMAKE_CXX_STANDARD 20)

add_subdirectory(third_party)

# Examples

# Hand-crafted stackless coroutines

add_subdirectory(state_machine)
add_subdirectory(state_machine_2)
add_subdirectory(state_machine_3)
add_subdirectory(state_machine_4)

# C++20 coroutines

add_subdirectory(timers)
add_subdirectory(thread_pool)
add_subdirectory(await_futures)
add_subdirectory(echo)
add_subdirectory(compose)
